const Course = require('../Models/coursesSchema');
const auth = require('../auth.js');

module.exports.addCourse = (request, response) => {
  let input = request.body;

  const decoded = auth.decode(request.headers.authorization);

  if (decoded === null || !decoded.isAdmin) {
    return response.send({ auth: 'Failed. Only admin can create a course.' });
  } else {
    let newCourse = new Course({
      name: input.name,
      description: input.description,
      price: input.price,
    });

    newCourse
      .save()
      .then((course) => {
        response.send(course);
      })
      .catch((error) => {
        response.send(error);
      });

    return response.send(true);
  }
};

//Create controller where in it will retreived all the courss (active/ inactive courses)

module.exports.allCourses = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  if (!userData.isAdmin) {
    return response.send("You  don't have access to this route!");
  } else {
    Course.find({})
      .then((result) => response.send(result))
      .catch((error) => response.send(error));
  }
};

// Create a controller wherin it will retrieve courses that are active.
module.exports.allActiveCourses = (request, response) => {
  Course.find({ isActive: true })
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};

// mini activity
// Where are going to create a route wherein it will can retreive all inactive course/
//  make sure that the admin users only are the onse that can access

// Create a controler where it will retrive courses that are not active.
module.exports.allNotActiveCourses = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  if (userData === null || !userData.isAdmin) {
    return response.send({
      auth: 'Failed. Only admin can access inactive courses.',
    });
  } else {
    Course.find({ isActive: false })
      .then((result) => response.send(result))
      .catch((error) => response.send(error));
  }
};

// This controller will get the details of specific course.
module.exports.courseDetails = (request, response) => {
  //to get the params from the url
  const courseId = request.params.courseId;

  Course.findById(courseId)
    .then((result) => response.send(result))
    .catch((error) => response.send(erorr));
};

// This controller is for updating specific course
/*
  Logic:
  1. We are going to edit/update the course, that is
  stored in the params.
*/

module.exports.updateCourse = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  const courseId = request.params.courseId;

  const input = request.body;

  if (!userData.isAdmin) {
    return response.send("You don't have acces in this page!");
  } else {
    await Course.findOne({ _id: courseId }).then((result) => {
      if (result === null) {
        return response.send('courseId is invalid, please try again!');
      } else {
        let updatedCourse = {
          name: input.name,
          description: input.description,
          price: input.price,
        };

        Course.findByIdAndUpdate(courseId, updatedCourse, { new: true })
          .then((result) => {
            console.log(result);
            return response.send(result);
          })
          .catch((error) => response.send(error));
      }
    });
  }
};

// Archive courses
module.exports.archiveCourse = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const courseId = request.params.courseId;

  if (!userData.isAdmin) {
    return response.send("You don't have access to this page!");
  } else {
    await Course.findOne({ _id: courseId }).then((result) => {
      if (result === null) {
        return response.send('courseId is invalid, please try again!');
      } else {
        Course.findByIdAndUpdate(courseId, { isActive: false }, { new: true })
          .then((result) => {
            console.log(result);
            return response.send(result);
          })
          .catch((error) => response.send(error));
      }
    });
  }
};
