const mongoose = require('mongoose');
const User = require('../Models/usersSchema.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Course = require('../Models/coursesSchema');

// Controllers

// This controller will create or register a user on our db/database
module.exports.userRegistration = (request, response) => {
  const input = request.body;

  User.findOne({ email: input.email })
    .then((result) => {
      if (result !== null) {
        return response.send('The email is already taken!.');
      } else {
        let newUser = new User({
          firstName: input.firstName,
          lastName: input.lastName,
          email: input.email,
          password: bcrypt.hashSync(input.password, 10),
          mobileNo: input.mobileNo,
        });
        newUser
          .save()
          .then((save) => {
            return response.send('You are now registered to our website!');
          })
          .catch((error) => {
            return response.send(error);
          });
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};

// User Authentication
module.exports.userAuthentication = (request, response) => {
  let input = request.body;

  // possbile scenarios in logging in
  // 1. emial is not yet registered.
  // 2. email is regsitered but the password is wrong

  User.findOne({ email: input.email })
    .then((result) => {
      if (result === null) {
        return response.send(
          'Email is not yet registered. Register first before logging in'
        );
      } else {
        // we have to verify if the password is correct.
        // the compareSync method is used to compare a non ecrypted password to the encrypted password.
        // it returns boolean value, if match true value is return otehrwise false.
        const isPasswordCorrect = bcrypt.compareSync(
          input.password,
          result.password
        );

        if (isPasswordCorrect) {
          return response.send({ auth: auth.createAccessToken(result) });
        } else {
          return response.send('Password is incorrect');
        }
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};

module.exports.getProfile = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  console.log(userData);

  return User.findById(userData._id).then((result) => {
    result.password = '';

    return response.send(result);
  });
};

// controlelr for enrollment
// 1. get id by user by decoding jwt
// 2. We can get the courseId by using the request params

module.exports.enrollCourse = async (request, response) => {
  // First we have to get userId and the courseId

  //decode the token to extract/unpack the payload

  const userData = auth.decode(request.headers.authorization);

  const courseId = request.params.courseId;

  // check if the course ID is valid
  const course = await Course.findById(courseId);
  if (!course) {
    return response.send('Invalid course ID.');
  }

  // 2 thing we need to do in this controller
  // First  to push the courseId in the enrollments property of the user
  // Second to push the userId in the erolles property of the course

  let isUserUpdated = await User.findById(userData._id).then((result) => {
    if (result === null || result.isAdmin) {
      return response.send('You are not authorized to enroll in courses');
    } else {
      result.enrollments.push({ courseId: courseId });

      return result
        .save()
        .then((save) => true)
        .catch((error) => false);
    }
  });

  let isCourseUpdated = await Course.findById(courseId).then((result) => {
    if (result === null) {
      return false;
    } else {
      result.enrollees.push({ userId: userData._id });

      return result
        .save()
        .then((save) => true)
        .catch((error) => false);
    }
  });

  if (isCourseUpdated && isUserUpdated) {
    return response.send('The course is now enrolled!');
  } else {
    return response.send('There was an error during the ernollment.');
  }
};
