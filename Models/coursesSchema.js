const mongoose = require('mongoose');

const coursesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name of the course is required!.'],
  },
  description: {
    type: String,
    required: [true, 'Description of the course is required!.'],
  },
  price: {
    type: Number,
    required: [true, 'Price of the course is required!.'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    // The" new date()" expression instantiates a new "date" that
    // stores the current date and timee whenrever a course is created in our database.
    default: new Date(),
  },
  enrollees: [
    {
      userId: {
        type: String,
        required: [true, 'UserId is required'],
      },
      enrollOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

module.exports = mongoose.model('Course', coursesSchema);
