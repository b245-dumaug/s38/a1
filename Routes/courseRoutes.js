const express = require('express');
const router = express.Router();
const courseController = require('../Controllers/courseController');
const auth = require('../auth.js');

// Route for creating a course
router.post('/', auth.verify, courseController.addCourse);

// Route for retriveing all courses
router.get('/all', auth.verify, courseController.allCourses);

// Route for retrieving all active courses
router.get('/allactive', courseController.allActiveCourses);

//Route for retrieving all not active
router.get('/allNotAcitve', auth.verify, courseController.allNotActiveCourses);

//Route for retrieving detail/s of specific course.
router.get('/:courseId', courseController.courseDetails);

//
router.put('/update/:courseId', auth.verify, courseController.updateCourse);

//archive course
router.put('/archive/:courseId', auth.verify, courseController.archiveCourse);

module.exports = router;
