const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const userController = require('../Controllers/useController.js');

//[ROUTES]
// this route is respnsible of the registration of the user.
router.post('/register', userController.userRegistration);

router.post('/login', userController.userAuthentication);

router.get('/details', auth.verify, userController.getProfile);

//router.post
router.post('/enroll/:courseId', auth.verify, userController.enrollCourse);

module.exports = router;
