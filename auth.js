const jwt = require('jsonwebtoken');

const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {
  // will contain the data that will be passed to other parts of ou API
  const data = {
    _id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret);
};

module.exports.verify = (request, response, next) => {
  let token = request.headers.authorization;

  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);
    console.log(token);

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return response.send({ auth: 'Failed.' });
      } else {
        next();
      }
    });
  } else {
    return response.send({ auth: 'Failed.' });
  }
};

module.exports.decode = (token) => {
  // Token received is not undefined
  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
