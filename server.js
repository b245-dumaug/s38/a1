const express = require('express');
const mongoose = require('mongoose');

//by default our backend's CORS setting will prevent any request.Using the cors package, it
// will allowr us to manipulate this and control what applications may use our app.

// Alloows our backend application to be available to our
// frontend application
// Allows us to control the app's Cross origin Resouse Sharing
const cors = require('cors');

const userRoutes = require('./Routes/userRoutes.js');
const courseRoutes = require('./Routes/courseRoutes.js');

const app = express();

mongoose.set('strictQuery', true);

const port = 3002;

mongoose.connect(
  'mongodb+srv://admin:admin@batch245-dumaug.18ijatd.mongodb.net/batch245_Course_API_Dumaug?retryWrites=true&w=majority',
  {
    // Allows us to avoid any acurrent and future errors while connecting to mongodb.
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// check connection
let db = mongoose.connection;

//error catcher
db.on('error', console.error.bind(console, 'Connection Error!'));

//Confirmation of the connection
db.once('open', () => console.log('We are now connected to the cloud!'));

// NOTE: Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// routing
app.use('/user', userRoutes);
app.use('/course', courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));
